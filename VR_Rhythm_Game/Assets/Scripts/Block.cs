using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Blue
}

public class Block : MonoBehaviour
{
    public BlockColor color;
    
    public GameObject brokenBlockLeft;
    public GameObject brokenBlockRight;
    public float brokenBlockForce;
    public float brokenBlockTorque;
    public float brokenBlockDestroyDelay;
    
    void OntriggerEnter(Collider other)
    {
        if (other.CompareTag("SwordLeft"))
        {
            if(color == BlockColor.Green && GameManager.instance.leftSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
            //add to score
                GameManager.instance.AddScore();
            }
            else
            {
            // subtract a life
                GameManager.instance.hitWrongBlock();
            }
            
            Hit();
        }
        else if(other.CompareTag("SwordRight"))
        {
            if(color == BlockColor.Blue && GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
            //add to score
                GameManager.instance.AddScore();
            }
            else
            {
            // subtract a life
                GameManager.instance.hitWrongBlock();
            }
            
            Hit();
        }
    }   
    
    public void Hit()
    {
        //enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);
        
        // remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;
        
        //add force to them
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
        
        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
        
        // add torque to them
        leftRig.AddTorque(-transform.forward * brokenBlockTorque,ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque,ForceMode.Impulse);
        
        //destroy the broken peices after a few seconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);
        
        // destroy the main block
        Destroy(gameObject);

    }
}
