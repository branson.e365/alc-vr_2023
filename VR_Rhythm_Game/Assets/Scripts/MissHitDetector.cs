using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissHitDetector : MonoBehaviour
{

    void OnTriggerEnter (Collider other)
    {
        if(other.CompareTag("Block"))
        {
            other.GetComponent<Block>().Hit();
            GameManager.instance.MissBlock();
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
